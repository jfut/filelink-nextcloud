/* MIT License

Copyright (c) 2020 Johannes Endres

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the "Software"), to deal in
the Software without restriction, including without limitation the rights to
use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
the Software, and to permit persons to whom the Software is furnished to do so,
subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE. */

const accountId = new URL(location.href).searchParams.get("accountId");
const ncc = new CloudConnection(accountId);

loadFormData()
    .then(updateHeader)
    .then(showErrors);

addLocalizedLabels();

linkButtonStateToFieldChanges();

serverUrl.addEventListener("input", updateHeader);
username.addEventListener("input", updateGauge);

linkElementStateToCheckbox(downloadPassword, useDlPassword);
linkElementStateToCheckbox(expiryDays, useExpiry);


//#region html element event handlers
/**
 * Save button is only active if field values validate OK
 * Reset button is only active if any field has been changed
 */
async function linkButtonStateToFieldChanges() {
    function updateButtons() {
        saveButton.disabled = !accountForm.checkValidity();
        resetButton.disabled = false;
    }
    accountForm.addEventListener('input', updateButtons);
}

/**
 *  enable/disable text input field according to checkbox state
 */
async function linkElementStateToCheckbox(element, checkbox) {
    checkbox.addEventListener("input", async () => {
        element.disabled = !checkbox.checked;
        element.required = !element.disabled;
    });
}

/** 
 * Handler for Cancel button, restores saved values
 */
resetButton.addEventListener('click', () => {
    popup.clear();
    loadFormData()
        .then(updateHeader);
    resetButton.disabled = saveButton.disabled = true;
});

/** Handler for Save button */
saveButton.addEventListener('click', () => {
    saveButton.disabled = resetButton.disabled = true;
    Promise.all([lookBusy(), handleFormData(), popup.clear(),])
        .then(() => {
            updateHeader();
            stopLookingBusy();
        });
});

//#endregion
//#region Fill visible html elements with content
/**
 * Set all the labels to localized strings
 */
async function addLocalizedLabels() {
    document.querySelectorAll("[data-message]")
        .forEach(element => {
            element.innerHTML = browser.i18n.getMessage(element.dataset.message);
        });
}

/**
 * Display cloud type (as a logo) and version
 */
async function showVersion() {
    service_url.href = serverUrl.value.trim();

    if (serverUrl.value.trim() === ncc.serverUrl && 'undefined' !== typeof ncc.cloud_supported) {
        cloud_version.textContent = ncc.cloud_versionstring;
        provider_name.textContent = ncc.cloud_productname || '*cloud';
        logo.src = {
            "Nextcloud": "images/nextcloud-logo.svg",
            "ownCloud": "images/owncloud-logo.svg",
            "Unsupported": "images/management.png",
        }[ncc.cloud_type];

        if (!ncc.cloud_supported) {
            obsolete_string.hidden = false;
        }
    } else {
        provider_name.textContent = '*cloud';
        logo.src = "images/management.png";
        cloud_version.textContent = "";
    }
}

/**
 * Update free space gauge
 */
async function updateGauge() {
    // Only show gauge if relevant form data match the account data
    if (username.value !== ncc.username || serverUrl.value !== ncc.serverUrl) {
        freespaceGauge.style.visibility = "hidden";
    } else {
        let theAccount = await messenger.cloudFile.getAccount(accountId);
        // Update the free space gauge
        let free = theAccount.spaceRemaining;
        const used = theAccount.spaceUsed;
        if (free >= 0 && used >= 0) {
            const full = (free + used) / (1024.0 * 1024.0 * 1024.0); // Convert bytes to gigabytes
            free /= 1024.0 * 1024.0 * 1024.0;
            freespacelabel.textContent = browser.i18n.getMessage("freespace", [
                free > 100 ? free.toFixed() : free.toPrecision(2),
                full > 100 ? full.toFixed() : full.toPrecision(2),]);
            freespace.max = full;
            freespace.value = free;
            freespace.low = full / 10;
            freespaceGauge.style.visibility = "visible";
        }
    }
}

function updateHeader() {
    return Promise.all([showVersion(), updateGauge(),]);
}

function showErrors() {
    if (false === ncc.public_shares_enabled) {
        popup.error('sharing_off');
    } else {
        if (ncc.enforce_password && (!useDlPassword.checked || !downloadPassword.value)) {
            popup.error('password_enforced');
        }
        if (ncc.invalid_downloadpassword_reason) {
            popup.error('invalid_pw', ncc.invalid_downloadpassword_reason);
        }
        if (false === ncc.cloud_supported) {
            popup.warn('unsupported_cloud');
        }
    }
}

/**
 * Load stored account data into form
 */
async function loadFormData() {
    await ncc.load();

    document.querySelectorAll("input")
        .forEach(inp => {
            if (inp.type === "checkbox") {
                inp.checked = !!ncc[inp.id];
            } else if (ncc[inp.id]) {
                inp.value = ncc[inp.id];
            }
        });

    // Don't allow longer expiry period than the server
    if (ncc.expiry_max_days) {
        expiryDays.max = ncc.expiry_max_days;
    }

    // force download password if server requires one
    if (ncc.enforce_password) {
        useDlPassword.checked = true;
        useDlPassword.disabled = true;
    }

    downloadPassword.disabled = !useDlPassword.checked;
    downloadPassword.required = useDlPassword.checked;

    expiryDays.disabled = !useExpiry.checked;
    expiryDays.required = useExpiry.checked;
}
//#endregion

//#region Helpers
/**
 * Part of the save button handler
 */
async function handleFormData() {

    sanitizeInput();

    // If user typed new password, username or URL the token is likely not valid any more
    const needsNewToken = password.value !== ncc.password ||
        username.value !== ncc.username ||
        serverUrl.value !== ncc.serverUrl;

    copyData();

    // Try to convert the password into App Token if necessary
    if (needsNewToken) {
        ncc.convertToApppassword()
            .then(pw => { password.value = pw; });
    }

    // Get info from cloud
    await updateCloudInfo();
    await ncc.updateConfigured();
    ncc.store();

    showErrors();
    if (popup.empty()) {
        popup.success();
    }
    // Done. Now internal functions

    function sanitizeInput() {
        document.querySelectorAll("input")
            .forEach(element => {
                element.value = element.value.trim();
            });
        storageFolder.value = "/" + storageFolder.value.split('/').filter(e => "" !== e).join('/');

        const url = new URL(serverUrl.value);
        const shortpath = url.pathname.split('/').filter(e => "" !== e);

        if (shortpath[shortpath.length - 1] === 'files' && shortpath[shortpath.length - 2] === 'apps') {
            shortpath.pop();
            shortpath.pop();
            if (shortpath[shortpath.length - 1] === 'index.php') {
                shortpath.pop();
            }
        }
        serverUrl.value = url.origin + '/' + shortpath.join('/');
        if (!serverUrl.value.endsWith('/')) {
            serverUrl.value += '/';
        }
    }

    /**
     * Copy data into the connection object
     */
    function copyData() {
        document.querySelectorAll("input")
            .forEach(inp => {
                if (inp.type === "checkbox") {
                    ncc[inp.id] = inp.checked;
                }
                else {
                    ncc[inp.id] = inp.value;
                }
            });
    }

    /**
     * Try login data by fetching Quota. If that succeeds, get capabilities and
     * store them in the Cloudconnection object,inform user about it.
     */
    async function updateCloudInfo() {
        const answer = await ncc.updateFreeSpaceInfo();
        if (answer._failed) {
            popup.error(answer.status);
        }
        else {
            ncc.forgetCapabilities();
            await getCapabilities();
        }

        // Inner functions of getCloudInfo
        /**
         * Get capabilities from cloud, change input form to meet policies, inform user
         */
        async function getCapabilities() {
            await ncc.updateCapabilities();
            if (false === ncc.public_shares_enabled) {
                popup.error('sharing_off');
            } else if (true === ncc.public_shares_enabled) {
                let account_ok = true;
                account_ok = checkEnforcedExpiry(account_ok);
                account_ok = checkEnforcedDLPassword(account_ok);
                account_ok = await validateDLPassword(account_ok);
                if (false === ncc.cloud_supported) {
                    popup.warn('unsupported_cloud');
                    account_ok = false;
                }
                if (true === account_ok) {
                    popup.success();
                } else {
                    ncc.store();
                }
            } else {
                popup.warn('no_config_check');
            }

            /**
             * Try to validate download password
             * AFAIK this only works with NC >=17, so ignore all errors.
             */
            async function validateDLPassword() {
                delete ncc.invalid_downloadpassword_reason;
                if (useDlPassword.checked) {
                    const result = await ncc.validateDLPassword();
                    if (false === result.passed) {
                        ncc.invalid_downloadpassword_reason = result.reason || '(none)';
                    }
                }
            }

            /**
             * If password is enforced, make it mandatory by changing the inputs
             */
            function checkEnforcedDLPassword() {
                if (ncc.enforce_password && !useDlPassword.checked) {
                    useDlPassword.checked = true;
                    useDlPassword.disabled = true;
                    downloadPassword.disabled = false;
                    downloadPassword.required = true;
                }
            }

            /**
             * Check for maximum expiry on server
             */
            function checkEnforcedExpiry() {
                const expiry_input = document.getElementById("expiryDays");
                if (ncc.expiry_max_days) {
                    expiryDays.max = ncc.expiry_max_days;
                    if (parseInt(expiryDays.value) > ncc.expiry_max_days) {
                        expiryDays.value = ncc.expiry_max_days;

                        ncc.expiryDays = ncc.expiry_max_days;
                        popup.warn('expiry_too_long', ncc.expiry_max_days);
                    }
                } else {
                    expiry_input.removeAttribute('max');
                }
            }
        }
    }
}

/**
* Set the busy cursor and deactivate all inputs
*/
async function lookBusy() {
    document.querySelector("body").classList.add('busy');
    disableable_fieldset.disabled = true;
}

/**
 * Hide the busy cursor and reactivate all fields that were active
 */
function stopLookingBusy() {
    disableable_fieldset.disabled = false;
    document.querySelector("body").classList.remove('busy');
}
//#endregion
// Make jshint happy
// Defined in ../lib/cloudconnection.js
/* global CloudConnection */
// Defined in popup/popup.js
/* global popup */
// Defined in managemet.html as id
/* globals serverUrl, username, downloadPassword, useDlPassword, expiryDays */
/* globals useExpiry, saveButton, accountForm, resetButton, service_url */
/* globals provider_name, logo, cloud_version, obsolete_string, freespaceGauge */
/* globals freespacelabel, freespace, password, storageFolder, disableable_fieldset */